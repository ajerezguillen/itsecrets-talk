#!/usr/bin/php -q
<?php 
require("vendor/autoload.php");
require("config.php");

$queue = new Pheanstalk_Pheanstalk('127.0.0.1', 11300);

// Set which queues to bind to
$queue->watch("itsecrets");

// pick a job and process it
while(1) {
    $job = $queue->reserve();
    if ($job) {
        $received = json_decode($job->getData(), true);
        $input = $received['input'];
        $output = $received['output'];
        $percent = $received['percent'];


        echo "Received a Resize Job, Resize ".$percent."%  (" . $input ." ==> ". $output .") ...".PHP_EOL;


        // Get new dimensions
        list($width, $height) = getimagesize($input);
        $new_width = $width * $percent;
        $new_height = $height * $percent;

        $image_p = imagecreatetruecolor($new_width, $new_height);
        $image = imagecreatefromjpeg($input);

        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

        // Output
        if (imagejpeg($image_p, $output, 100)){
            echo "done \n";
            $queue->delete($job);
       
        } else {
            echo "bury \n";
            $queue->bury($job);
        }
        #$queue->delete($job);
    }
    #sleep(0.2);
}

?>

