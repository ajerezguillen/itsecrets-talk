<?php

require("vendor/autoload.php");

$filename = './imgs/img1.jpg';
$imgThumb = './imgs/thumbs/thumb'.rand(0,99).'.jpg';

$percent = 0.5;

$queue = new Pheanstalk_Pheanstalk($GLOBALS['beanstalkdhost'],$GLOBALS["beanstalkdport"]);

$job = array('input' => $filename,
             'output' => $imgThumb,
             'percent' => $percent);

$queue->useTube('itsecrets')->put(json_encode($job));

?>
