# README #

Aquí podéis encontrar parte del código Punk que utilice en la presentación del programa I.T. Secrets Exposed en las oficinas de Freepik, Málaga, 9/12/2015

### ¿Qué hay en este repo? ###

* Esqueleto básico de una app web, la cual se utilizó en la presentación para simular carga de trabajo.
* index.php conecta con la bbdd, ejecuta 10 queries, y carga una plantilla bootstrap básica.
* resizeimage.php redimensiona al vuelo la imagen situada en /imgs/img.jpg
* resizeimagequeue.php encola una tarea de redimensionamiento en beanstalk
* worker.php demonio php que se ejecutará con supervisor para redimensionar las imágenes asincronamente.
* php/clases/database.php ejemplo básico de balanceo de consultas por código.

En el directorio tsung, se pueden encontrar varias configuraciones para benchmarking, las utilizadas en la presentación fueron:

* tsung/aws/awsMediumWithJpg.xml
* tsung/aws/awsMediumWithQueue.xml
* tsung/aws/awsHeavy02.xml

###  ¿Como reproduzco los ejemplos ? ###
Vas a necesitar tener  instalado un stack LEMP, esto es: Nginx, MySQL & PHP

* Ejecuta composer update en la raíz del repo.
* Instala beanstalkd " apt-get install beanstalkd "
* Configura las ips de los distintos nodos ( maestro mysql, esclavo mysql, beanstalk )
* Vuelca en la base de datos, el esquema Sakila: 
* Para ejecutar los tests, ejecutar tsung -f tsung/aws/awsMediumWithJpg.xml start

### Código de ejemplo a efectos de enseñar ciertas técnicas ###

Esté código dista muchísimo de ser usable en producción, se trata del código de ejemplo que escribí para apoyar la presentación de distintas técnicas de programación en entornos de alto rendimiento. Las transparencias de apoyo a dicha charla pueden encontrarse en el siguiente enlace:

[Transparencias](https://dl.dropboxusercontent.com/u/232422/Slides%20I.T.%20Secrets%20%40%20Freepik%20HQ%20-%202.pdf)


### Autor ###
[Antonio Jerez](http://www.antoniojerez.com/)
[El programa I.T. Secrets](http://itsecrets.antoniojerez.com/)