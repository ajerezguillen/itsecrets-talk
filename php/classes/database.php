<?php

class database {
    // The database connection
    protected static $db;
    protected static $dbslave;

    /**
     * Connect to the database
     * 
     * @return bool false on failure / pdo obejct instance on success
     */

    public function __construct(){
        $this->masterhost = $GLOBALS["masterhost"];
        $this->slavehost = $GLOBALS["slavehost"];
        $this->slaveweigth = $GLOBALS["slaveweigth"];

        $this->dbuser = $GLOBALS["dbuser"];
        $this->dbname = $GLOBALS["dbname"];
        $this->dbpass = $GLOBALS["dbpass"];


        $this->connect();
    }

    public function connect() {    
        // Try and connect to the database
        if(!isset(self::$db)) {
            self::$db = new PDO(
                        'mysql:host='.$this->masterhost.';dbname='.$this->dbname.';charset=utf8', 
                        $this->dbuser, 
                        $this->dbpass
                        );       
        }

        if(self::$db === false) {
            return false;
        }


        if(!isset(self::$dbslave) && isset($this->slavehost)) {

            self::$dbslave = new PDO(
                        'mysql:host='.$this->slavehost.';dbname='.$this->dbname.';charset=utf8',
                        $this->dbuser,
                        $this->dbpass
                        );

            //self::$slaveweigth = 75;
        }

        return true;
    }

    /**
     * Query the database
     *
     * @param $query The query string
     * @return mixed The result of the mysqli::query() function
     */
    public function query($query) {
        // Connect to the database


        $seed = rand(0,99);
        $select = (stripos($query, 'ELECT') == 1) ? true : false;

        if ( $select && self::$dbslave && ($this->slaveweigth >= $seed)) {
            echo "SLAVE".PHP_EOL;
            $db = self::$dbslave;
        }else{
            echo "MASTER".PHP_EOL;
            $db = self::$db;
        }
        // Query the database
        $result = $db->query($query);

        return $result;
    }

    /**
     * Fetch rows from the database (SELECT query)
     *
     * @param $query The query string
     * @return bool False on failure / array Database rows on success
     */
    public function select($query) {
        $rows = array();
        $result = $this -> query($query);
        if($result === false) {
            return false;
        }
        while ($row = $result -> fetch()) {
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * Fetch the last error from the database
     * 
     * @return string Database error message
     */
    public function error() {
        $connection = $this -> connect();
        return $connection -> error;
    }

}

?>
