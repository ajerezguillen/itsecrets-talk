<?php
session_start();
include("php/classes/database.php");
include("config.php");
$mydb = new database();

$queries = array ("SELECT * FROM film;",
                  "SELECT CONCAT(customer.last_name, ', ', customer.first_name) AS customer, address.phone, film.title
                        FROM rental INNER JOIN customer ON rental.customer_id = customer.customer_id
                        INNER JOIN address ON customer.address_id = address.address_id
                        INNER JOIN inventory ON rental.inventory_id = inventory.inventory_id
                        INNER JOIN film ON inventory.film_id = film.film_id
                        WHERE rental.return_date IS NULL
                        AND rental_date + INTERVAL film.rental_duration DAY < CURRENT_DATE()
                        LIMIT 5;",
                  "SELECT F.title, F.description, A.first_name, A.last_name
                        FROM film F
                        JOIN film_actor FA ON FA.film_id=F.film_id
                        JOIN actor A ON A.actor_id=FA.actor_id
                        ORDER BY RAND
                        LIMIT 200;",
                  "SELECT * FROM film;",
                  "SELECT CONCAT(customer.last_name, ', ', customer.first_name) AS customer, address.phone, film.title
                        FROM rental INNER JOIN customer ON rental.customer_id = customer.customer_id
                        INNER JOIN address ON customer.address_id = address.address_id
                        INNER JOIN inventory ON rental.inventory_id = inventory.inventory_id
                        INNER JOIN film ON inventory.film_id = film.film_id
                        WHERE rental.return_date IS NULL
                        AND rental_date + INTERVAL film.rental_duration DAY < CURRENT_DATE()
                        LIMIT 5;",
                  "SELECT F.title, F.description, A.first_name, A.last_name
                        FROM film F
                        JOIN film_actor FA ON FA.film_id=F.film_id
                        JOIN actor A ON A.actor_id=FA.actor_id
                        ORDER BY RAND
                        LIMIT 200;",
                  "SELECT * FROM film;",
                  "SELECT CONCAT(customer.last_name, ', ', customer.first_name) AS customer, address.phone, film.title
                        FROM rental INNER JOIN customer ON rental.customer_id = customer.customer_id
                        INNER JOIN address ON customer.address_id = address.address_id
                        INNER JOIN inventory ON rental.inventory_id = inventory.inventory_id
                        INNER JOIN film ON inventory.film_id = film.film_id
                        WHERE rental.return_date IS NULL
                        AND rental_date + INTERVAL film.rental_duration DAY < CURRENT_DATE()
                        LIMIT 5;",
                  "SELECT F.title, F.description, A.first_name, A.last_name
                        FROM film F
                        JOIN film_actor FA ON FA.film_id=F.film_id
                        JOIN actor A ON A.actor_id=FA.actor_id
                        ORDER BY RAND
                        LIMIT 200;"

);

foreach ( $queries as $query ) {
    $rows = $mydb->select($query);
}

include("templates/home.html");
?>
